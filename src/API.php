<?php
namespace Hsalubre\TigerWebCensus;

use GuzzleHttp\Client as Guzzle;
use GuzzleHttp\Exception\RequestException;
/*
 * This file is part of the dom package.
 *
 * (c)  COPYRIGHT DOM360
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * DOM360 TigerWebCensus
 *
 * @note - invistigate this link http://tigerweb.geo.census.gov/arcgis/services/TIGERweb/tigerWMS_Current/MapServer/WMSServer?service=WMS&states=3301&request=getmap
 * @package DOM
 * @author Hasmer Salubre <salubrehasmer@gmail.com>
 * @access public
 */
class API{
    private $client;
    private $contents;
    private $params=['endPoint'=>'us'];

    private $url='http://tigerweb.geo.census.gov/arcgis/rest/services/tigerWMS_Current/MapServer/2/query?where=&text=%s&objectIds=&time=&geometry=&geometryType=esriGeometryPolygon&inSR=&spatialRel=esriSpatialRelIntersects&relationParam=&outFields=&returnGeometry=true&maxAllowableOffset=&geometryPrecision=&outSR=&returnIdsOnly=true&returnCountOnly=false&orderByFields=&groupByFieldsForStatistics=&outStatistics=&returnZ=false&returnM=false&gdbVersion=&returnDistinctValues=false&f=pjson';    
    
    public $mapID=null;
    public function __construct(){
        $this->client=new Guzzle();
    }

    /**
     * @param  [string] $value [centerLongitude]
     */
    private function setcenterLongitude($value){
        $valid=false;
        if($this->isLatitude(($value))){
            $this->params['centerLongitude']=$value;
        }
        if(!$valid) throw new \InvalidArgumentException("Invalid center longitude");
    }
    /**
     * @param  [string] $value [intLatitude]
     */
    private function setintLongitude($value){
        $valid=false;
        if($this->isLongitude($value)){
            $this->params['intLongitude']=$value;
        }
        if(!$valid) throw new \InvalidArgumentException("Invalid int longitude");
    }
    /**
     * @param  [string] $value [intLatitude]
     */
    private function setintLatitude($value){
        $valid=false;
        if($this->isLatitude($value)){
            $this->params['intLatitude']=$value;
        }
        if(!$valid) throw new \InvalidArgumentException("Invalid int latitude");
    }
    /**
     * @param  [string] $value [centerLatitude]
     */
    private function setcenterLatitude($value){
        $valid=false;
        if($this->isLatitude($value)){
            $this->params['centerLatitude']=$value;
        }
        if(!$valid) throw new \InvalidArgumentException("Invalid center latitude");
    }
    /**
     * @param  [string] $value [message]
     */
    private function setzip($value){
        $this->matchPattern($value,"/^[0-9]{0,5}$/","zip");          
    }
    /**
     * @param  [string] $value [mapID]
     */
    private function setmapID($value){
        $this->matchPattern($value,"/^[0-9]{0,5}$/","mapID");          
    }
    
    /**
     * tigerweb::getPolygon() - get and set the polygon coordinates
     * 
     * @return geometrics else false
     */
    public function getPolygon(){
        $this->url=sprintf($this->url,$this->zipCode);
        $res=$this->request($this->url);
        $res=json_decode($res);        
        $mapID=$res->objectIds[0];
        echo var_dump($mapID);
        $res=file_get_contents("http://tigerweb.geo.census.gov/arcgis/rest/services/tigerWMS_Current/MapServer/2/".$mapID."?f=pjson");
        $res=json_decode($res);
        $this->centLat=$res->feature->attributes->CENTLAT;
        $this->centLon=$res->feature->attributes->CENTLON;
        $this->intLat=$res->feature->attributes->INTPTLAT;
        $this->intLon=$res->feature->attributes->INTPTLON;
        if(isset($res->feature->geometry->rings[0])){
            $geometrics=$res->feature->geometry->rings[0];            
            return $geometrics;
        }
        return false;
    }
    /**
    * use the guzzle client to rqeuest data from params
    * @param string $path
    * @return boolean
    */
    public function request($path,$method='GET',$dataType='query',$body=null){
        try{
            $method=strtoupper($method);
            $params[$dataType]=$this->params;
            $params['verify']=false;
            if(!is_null($body)) $params['body']=$body;
            $response=$this->client->request($method,$path,$params);
            if($response->getReasonPhrase()=="OK"){
                $result=$response->getBody()->getContents();
                if(!empty($result)){
                    $this->contents=$result;
                    if(isset($this->params['oauth2_access_token'])){
                        foreach($this->params as $k=>$v){
                            if($k!=='key' ) unset($this->params[$k]);
                        }
                    }
                    return true;
                }
            }
        } catch (RequestException $e) {
            echo $e->getMessage();
        }
        return false;
    }
    public function __set($name, $value){
        $method='set'.$name;
        if (\method_exists($this,$method)){
            $this->$method($value);
        }else{
            throw new \InvalidArgumentException("Invalid property of openstreepapi class");
        }
    }

    public function __get($name){
        $method='get'.$name;
        if (\method_exists($this,$method)){
            return $this->$method();
        }
        return null;
    }

    /**
     * possible values - json,object,array
     * 
     * @param type $format - default json
     * @return type
     */
    public function getContents($format='json'){
        $results=$this->contents;
        if(is_string($results)){
            if($format=='object'){
                $results=json_decode("{$results}");
            }
            if($format=='array'){
                $results=json_decode("{$results}", true);
            }
        }
        return $results;
    }
    public function matchPattern(&$value,$pattern,$key,$customMessage=''){
        $value=(string)$value;
        $value=trim($value);
        $valid=false;
        if(preg_match($pattern, $value,$new,PREG_OFFSET_CAPTURE)==true){
            if(!empty($new[0][0])){
                $this->params[$key]=$new[0][0];                
                $valid=true;
            }
        }
        if(!$valid) throw new \InvalidArgumentException("Invalid ".(isEmpty($customMessage))?$key:$customMessage);
    }  
    private function isLatitude(&$value){
        $pattern="/^(\+|-)?(?:90(?:(?:\.0{1,6})?)|(?:[0-9]|[1-8][0-9])(?:(?:\.[0-9]{1,6})?))$/";
        if(preg_match($pattern, $value,$new,PREG_OFFSET_CAPTURE)){
            if(!empty($new[0][0])){
                return true;
            }
        }
        return false;
    }
    
    private function isLongitude(&$value){
        $pattern="/^(\+|-)?(?:180(?:(?:\.0{1,6})?)|(?:[0-9]|[1-9][0-9]|1[0-7][0-9])(?:(?:\.[0-9]{1,6})?))$/";
        if(preg_match($pattern, $value,$new,PREG_OFFSET_CAPTURE)){
            if(!empty($new[0][0])){
                return true;
            }
        }
        return false;
    }
}